import react from "react";
import reactDom from "react-dom";
import SeasonDisplay from "./SeasonDisplay";
import Spinner from "./Spinner";

class App extends react.Component {
  constructor(props) {
    // Good place to do one-time set up
    super(props);

    // THIS IS THE ONLY TIME we do direct assignment to this.state
    // this.state = { lat: null, errorMsg: "" };
  }

  state = { lat: null, errorMsg: "" };

  renderContent() {
    if (this.state.errorMsg && !this.state.lat) {
      return <div> Error: {this.state.errorMsg}</div>;
    }

    if (!this.state.errorMsg && this.state.lat) {
      return <SeasonDisplay lat={this.state.lat} />;
    }

    return <Spinner text="Waiting for location confirmation" />;
  }

  // render function is requiered by React
  render() {
    // Avoid doing anything besides returning JSX
    return <div className="content-wrapper">{this.renderContent()}</div>;
  }

  componentDidMount() {
    // Another lifcycle method called when content becoms visible on screen
    // Good please for data loading
    console.log("Component was rendered");
    window.navigator.geolocation.getCurrentPosition(
      (position) => {
        // we called setState from react.Component
        this.setState({ lat: position.coords.latitude });
      },
      (err) => {
        this.setState({ errorMsg: err.message });
      }
    );
  }

  componentDidUpdate() {
    // Another lifcycle method called when component got updated
    // Good please to perform additional dataloading when state/props change
    console.log("Component was updated and it re-rendered");
  }

  componentWillUnmount() {
    // Another lifcycle method called when component is not longer shown
    // Good pleace to do cleanup (especially for non-React stuff)
  }
}

reactDom.render(<App />, document.querySelector("#root"));
